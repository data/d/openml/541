# OpenML dataset: socmob

https://www.openml.org/d/541

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.csv`](./dataset/tables/data.csv): CSV file with data
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

**Author**:   
**Source**: Unknown - Date unknown  
**Please cite**:   

17x17x2x2 tables of counts in GLIM-ready format used for the analyses
in Biblarz, Timothy J., and Adrian E. Raftery. 1993. "The Effects of
Family Disruption on Social Mobility." American Sociological Review
(In press). For further details of the data, see this reference.
Column 1 is father's occupation, coded as follows:
17. Professional, Self-Employed
16. Professional-Salaried
15. Manager
14. Salesman-Nonretail
13. Proprietor
12. Clerk
11. Salesman-Retail
10. Craftsman-Manufacturing
9. Craftsmen-Other
8. Craftsman-Construction
7. Service Worker
6. Operative-Nonmanufacturing
5. Operative-Manufacturing
4. Laborer-Manufacturing
3. Laborer-Nonmanufacturing
2. Farmer/Farm Manager
1. Farm Laborer
Column 2 is son's occupation, coded in the same way as father's.
Column 3 is family structure, coded 1=intact family background and
2=nonintact family background.
Column 4 is race, coded 1=white and 2=black.
Column 5 is counts for son's first occupation.
Column 6 is counts for son's current occupation.
The counts have been weighted to take account of the survey
design, which is why they are not integers.
************************************************************
***********************************************************
This file was constructed from publicly available data collected
by David Featherman and Robert Hauser in 1973: the "Occupational
Change in a Generation II" (OCG II) Survey. Permission is hereby given to
use the above data for non-commercial scholarly and teaching purposes.
If these data are used in a published article or book,
the authors, the original data (in the form given in
Biblarz and Raftery (1993), cited above), and StatLib should
all be acknowledged.


Information about the dataset
CLASSTYPE: numeric
CLASSINDEX: none specific

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/541) of an [OpenML dataset](https://www.openml.org/d/541). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/541/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/541/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/541/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

